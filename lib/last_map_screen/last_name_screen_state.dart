import 'package:equatable/equatable.dart';

class AlertDialogState extends LastMapScreenState {}

class InitialState extends LastMapScreenState {}

abstract class LastMapScreenState extends Equatable {
  const LastMapScreenState();

  @override
  List<Object> get props => [DateTime.now().toIso8601String()];
}

class LaterLoginState extends LastMapScreenState {}

class LoadingState extends LastMapScreenState {}

class LoginEmailState extends LastMapScreenState {}

class LoginFacebookState extends LastMapScreenState {}

class LoginGoogleState extends LastMapScreenState {}

class OpenState extends LastMapScreenState {}

class SuccessState extends LastMapScreenState {}
