import 'dart:async';

import 'package:bloc/bloc.dart';

import 'last_name_screen_state.dart';

class LastMapScreenCubit extends Cubit<LastMapScreenState> {
  Map<String, StreamSubscription> subscriptions = {};

  final name = "lol";

  LastMapScreenCubit() : super(InitialState());

  @override
  Future<void> close() {
    _cancelSubscriptions();
    return super.close();
  }

  loginEmail() {
    emit(LoginEmailState());
  }

  loginFacebook() {
    emit(LoginFacebookState());
  }

  loginGoogle() {
    emit(LoginGoogleState());
  }

  void loginLater() {
    emit(LaterLoginState());
  }

  void ontap() {}

  void _cancelSubscriptions() {
    subscriptions.forEach((key, value) {
      value.cancel();
    });
  }
}
