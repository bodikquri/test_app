import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:test_app/generated/l10n.dart';
import 'package:test_app/modules/login/widgets/hint_last_screen.dart';
import 'package:test_app/modules/login/widgets/login_screen_widget_container.dart';
import 'package:test_app/modules/login/widgets/review_widget_last_screen.dart';
import 'package:test_app/modules/login/widgets/widget_for_last_screen.dart';

import '../gen/assets.gen.dart';
import '../resources/theme/app_theme.dart';

class LastMapScreen extends StatefulWidget {
  LastMapScreen({super.key});

  @override
  State<LastMapScreen> createState() => _LastMapScreenState();
}

class ReviewItem extends StatelessWidget {
  final String title;
  final String value;

  ReviewItem({required this.title, required this.value});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(title, style: TextStyle(fontSize: 16)),
        Text(value, style: TextStyle(fontSize: 16)),
      ],
    );
  }
}

class _LastMapScreenState extends State<LastMapScreen> {
  Gradient? containerGradient;
  double zoomValue = 12.0;
  double zoomChange = 1.0;
  GoogleMapController? mapController;

  double gravityValue = 55;

  @override
  Widget build(BuildContext context) {
    final colors = AppTheme.of(context).colors;
    final textTheme = AppTheme.of(context).textTheme;
    return Scaffold(
        backgroundColor: colors.background,
        body: ListView(children: [
          Stack(children: [
            Container(
              child: MyAssets.images.svg.lineForLocScreen.svg(),
            ),
            Container(
              child: MyAssets.images.svg.lineSecond.svg(),
            ),
            Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                    gradient: containerGradient,
                  ),
                  height: 378.h,
                  child: GoogleMap(
                    zoomControlsEnabled: false,
                    onTap: (LatLng latLng) {
                    },
                    onMapCreated: (controller) {
                      mapController = controller;
                    },
                    initialCameraPosition: CameraPosition(
                      target: LatLng(37.7749, -122.4194),
                      zoom: zoomValue,
                    ),
                  ),
                ),
                SizedBox(
                  height: 15.h,
                ),
                Container(
                  margin: EdgeInsets.only(left: 20.w),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(S.current.radiusMil,
                          style: textTheme.bodyMedium
                              ?.copyWith(color: colors.defaultColorText)),
                      SizedBox(
                        width: 16.w,
                      ),
                      InkWell(
                        onTap: () {
                          _updateZoom(zoomValue - zoomChange);

                        },
                        child: LastScreenContainers(
                            color: colors.colorContainerOnNamedScreen,
                            text: S.current.numberForLastScreenOne),
                      ),
                      SizedBox(
                        width: 13.w,
                      ),
                      InkWell(
                        onTap: () {
                          _updateZoom(zoomValue + zoomChange);
                        },
                        child: LastScreenContainers(
                          
                            color: colors.colorContainerOnNamedScreen,
                            text: S.current.numberForLastScreenSecond),
                      ),
                      SizedBox(
                        width: 13.w,
                      ),
                      InkWell(
                        onTap: () {
                          _updateZoom(zoomValue + zoomChange);
                        },
                        child: LastScreenContainers(
                            color: colors.colorContainerOnNamedScreen,
                            text: S.current.numberForLastScreenThree),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 20.h,
                ),
                ReviewLastScreen(),
                SizedBox(
                  height: 20.h,
                ),
                Container(
                  width: 350.w,
                  height: 130.h,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: colors.dropDownTextColor,
                          blurRadius: 4,
                          offset: Offset(2, 2),
                        ),
                      ],
                      color: colors.colorContainerOnNamedScreen,
                      borderRadius: BorderRadius.circular(10.sp)),
                  margin: EdgeInsets.only(left: 20.w, right: 20.w),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 19.w, top: 10.h),
                        alignment: Alignment.topLeft,
                        child: ShaderMask(
                          shaderCallback: (bounds) {
                            return LinearGradient(
                              colors: [
                                colors.colorForLinearOne,
                                colors.colorForLinearTwo
                              ],
                            ).createShader(bounds);
                          },
                          child: Text(S.current.hintLastScreen,
                              style: textTheme.caption
                                  .copyWith(color: colors.defaultColorText)),
                        ),
                      ),
                      SizedBox(
                        height: 10.h,
                      ),
                      HintLastScreen(),
                      SizedBox(
                        height: 10.h,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 25.h,
                ),
                WidgeContainerLoginScreen(
                  text: S.current.myGpsLastScreen,
                  image: MyAssets.images.svg.vector
                      .svg(color: colors.defaultColorText),
                  height: 0.h,
                )
              ],
            ),
          ]),
        ]));
  }

  void _updateZoom(double newZoom) {
    setState(() {
      zoomValue = newZoom;
      if (mapController != null) {
        mapController!.animateCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(
              target: LatLng(37.7749, -122.4194),
              zoom: zoomValue,
            ),
          ),
        );
      }
    });
  }
}
