// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Enter your phone number:`
  String get phoneNumber {
    return Intl.message(
      'Enter your phone number:',
      name: 'phoneNumber',
      desc: '',
      args: [],
    );
  }

  /// `55%`
  String get gravityvalue {
    return Intl.message(
      '55%',
      name: 'gravityvalue',
      desc: '',
      args: [],
    );
  }

  /// `Send code`
  String get sendCode {
    return Intl.message(
      'Send code',
      name: 'sendCode',
      desc: '',
      args: [],
    );
  }

  /// `Confirm Code`
  String get confirmCode {
    return Intl.message(
      'Confirm Code',
      name: 'confirmCode',
      desc: '',
      args: [],
    );
  }

  /// `+380992241071`
  String get sendVerificationCode {
    return Intl.message(
      '+380992241071',
      name: 'sendVerificationCode',
      desc: '',
      args: [],
    );
  }

  /// `111111`
  String get verifyCode {
    return Intl.message(
      '111111',
      name: 'verifyCode',
      desc: '',
      args: [],
    );
  }

  /// `Sign In`
  String get description {
    return Intl.message(
      'Sign In',
      name: 'description',
      desc: '',
      args: [],
    );
  }

  /// `Continue`
  String get textForContainer {
    return Intl.message(
      'Continue',
      name: 'textForContainer',
      desc: '',
      args: [],
    );
  }

  /// `Google`
  String get textForContainerWithIcon {
    return Intl.message(
      'Google',
      name: 'textForContainerWithIcon',
      desc: '',
      args: [],
    );
  }

  /// `Phone`
  String get textForContainerWithIconSecond {
    return Intl.message(
      'Phone',
      name: 'textForContainerWithIconSecond',
      desc: '',
      args: [],
    );
  }

  /// `or`
  String get textBetweenTwoContainer {
    return Intl.message(
      'or',
      name: 'textBetweenTwoContainer',
      desc: '',
      args: [],
    );
  }

  /// `Sign Out`
  String get textForSignOut {
    return Intl.message(
      'Sign Out',
      name: 'textForSignOut',
      desc: '',
      args: [],
    );
  }

  /// `First Name`
  String get firstNameNamedScreen {
    return Intl.message(
      'First Name',
      name: 'firstNameNamedScreen',
      desc: '',
      args: [],
    );
  }

  /// `Last Name`
  String get lastNameNamedScreen {
    return Intl.message(
      'Last Name',
      name: 'lastNameNamedScreen',
      desc: '',
      args: [],
    );
  }

  /// `Truck type`
  String get truckTypeNamedScreen {
    return Intl.message(
      'Truck type',
      name: 'truckTypeNamedScreen',
      desc: '',
      args: [],
    );
  }

  /// `You Are Here?`
  String get myLocation {
    return Intl.message(
      'You Are Here?',
      name: 'myLocation',
      desc: '',
      args: [],
    );
  }

  /// `What is your cargo price?`
  String get cargoPrice {
    return Intl.message(
      'What is your cargo price?',
      name: 'cargoPrice',
      desc: '',
      args: [],
    );
  }

  /// `Monitoring radius(mil)`
  String get radiusMil {
    return Intl.message(
      'Monitoring radius(mil)',
      name: 'radiusMil',
      desc: '',
      args: [],
    );
  }

  /// `Review:`
  String get reviewLastScreen {
    return Intl.message(
      'Review:',
      name: 'reviewLastScreen',
      desc: '',
      args: [],
    );
  }

  /// `Amount`
  String get amountLastScreen {
    return Intl.message(
      'Amount',
      name: 'amountLastScreen',
      desc: '',
      args: [],
    );
  }

  /// `Gravity`
  String get gravityLastScreen {
    return Intl.message(
      'Gravity',
      name: 'gravityLastScreen',
      desc: '',
      args: [],
    );
  }

  /// `Price`
  String get priceLastScreen {
    return Intl.message(
      'Price',
      name: 'priceLastScreen',
      desc: '',
      args: [],
    );
  }

  /// `Hint:`
  String get hintLastScreen {
    return Intl.message(
      'Hint:',
      name: 'hintLastScreen',
      desc: '',
      args: [],
    );
  }

  /// `Move to...`
  String get moveToLastScreen {
    return Intl.message(
      'Move to...',
      name: 'moveToLastScreen',
      desc: '',
      args: [],
    );
  }

  /// `Your GPS link`
  String get myGpsLastScreen {
    return Intl.message(
      'Your GPS link',
      name: 'myGpsLastScreen',
      desc: '',
      args: [],
    );
  }

  /// `33 unit`
  String get descriptionForAmount {
    return Intl.message(
      '33 unit',
      name: 'descriptionForAmount',
      desc: '',
      args: [],
    );
  }

  /// `0.75/$`
  String get descriptionForPrice {
    return Intl.message(
      '0.75/\$',
      name: 'descriptionForPrice',
      desc: '',
      args: [],
    );
  }

  /// `50`
  String get numberForLastScreenOne {
    return Intl.message(
      '50',
      name: 'numberForLastScreenOne',
      desc: '',
      args: [],
    );
  }

  /// `100`
  String get numberForLastScreenSecond {
    return Intl.message(
      '100',
      name: 'numberForLastScreenSecond',
      desc: '',
      args: [],
    );
  }

  /// `150`
  String get numberForLastScreenThree {
    return Intl.message(
      '150',
      name: 'numberForLastScreenThree',
      desc: '',
      args: [],
    );
  }

  /// `Continue with {variant}`
  String hello(String variant) {
    return Intl.message(
      'Continue with $variant',
      name: 'hello',
      desc: 'posible autorization',
      args: [variant],
    );
  }

  /// `Waypoint {number}`
  String waypoint(int number) {
    return Intl.message(
      'Waypoint $number',
      name: 'waypoint',
      desc: 'waypoint, with parametr number of point',
      args: [number],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'ua'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
