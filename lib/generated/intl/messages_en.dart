// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static String m0(variant) => "Continue with ${variant}";

  static String m1(number) => "Waypoint ${number}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "amountLastScreen": MessageLookupByLibrary.simpleMessage("Amount"),
        "cargoPrice":
            MessageLookupByLibrary.simpleMessage("What is your cargo price?"),
        "confirmCode": MessageLookupByLibrary.simpleMessage("Confirm Code"),
        "description": MessageLookupByLibrary.simpleMessage("Sign In"),
        "descriptionForAmount": MessageLookupByLibrary.simpleMessage("33 unit"),
        "descriptionForPrice": MessageLookupByLibrary.simpleMessage("0.75/\$"),
        "firstNameNamedScreen":
            MessageLookupByLibrary.simpleMessage("First Name"),
        "gravityLastScreen": MessageLookupByLibrary.simpleMessage("Gravity"),
        "gravityvalue": MessageLookupByLibrary.simpleMessage("55%"),
        "hello": m0,
        "hintLastScreen": MessageLookupByLibrary.simpleMessage("Hint:"),
        "lastNameNamedScreen":
            MessageLookupByLibrary.simpleMessage("Last Name"),
        "moveToLastScreen": MessageLookupByLibrary.simpleMessage("Move to..."),
        "myGpsLastScreen":
            MessageLookupByLibrary.simpleMessage("Your GPS link"),
        "myLocation": MessageLookupByLibrary.simpleMessage("You Are Here?"),
        "numberForLastScreenOne": MessageLookupByLibrary.simpleMessage("50"),
        "numberForLastScreenSecond":
            MessageLookupByLibrary.simpleMessage("100"),
        "numberForLastScreenThree": MessageLookupByLibrary.simpleMessage("150"),
        "phoneNumber":
            MessageLookupByLibrary.simpleMessage("Enter your phone number:"),
        "priceLastScreen": MessageLookupByLibrary.simpleMessage("Price"),
        "radiusMil":
            MessageLookupByLibrary.simpleMessage("Monitoring radius(mil)"),
        "reviewLastScreen": MessageLookupByLibrary.simpleMessage("Review:"),
        "sendCode": MessageLookupByLibrary.simpleMessage("Send code"),
        "sendVerificationCode":
            MessageLookupByLibrary.simpleMessage("+380992241071"),
        "textBetweenTwoContainer": MessageLookupByLibrary.simpleMessage("or"),
        "textForContainer": MessageLookupByLibrary.simpleMessage("Continue"),
        "textForContainerWithIcon":
            MessageLookupByLibrary.simpleMessage("Google"),
        "textForContainerWithIconSecond":
            MessageLookupByLibrary.simpleMessage("Phone"),
        "textForSignOut": MessageLookupByLibrary.simpleMessage("Sign Out"),
        "truckTypeNamedScreen":
            MessageLookupByLibrary.simpleMessage("Truck type"),
        "verifyCode": MessageLookupByLibrary.simpleMessage("111111"),
        "waypoint": m1
      };
}
