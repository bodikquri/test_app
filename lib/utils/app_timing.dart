mixin AppTiming {
  final Duration animateDuration = Duration(milliseconds: 350);
  final Duration tranzitionDuration = Duration(milliseconds: 200);
}
