import 'package:shared_preferences/shared_preferences.dart';

class SharedUtil {
  static final SharedUtil _instance = SharedUtil._internal();

  static const String accesToken = 'accesToken';

  static const String refreshToken = 'refreshToken';
  static const String userID = 'userID';
  static const String timeExpiresToken = 'timeExpiresToken';
  static const String userType = 'userType';
  factory SharedUtil() {
    return _instance;
  }

  SharedUtil._internal();

  Future<void> deleteTokens() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.remove(accesToken);
    await preferences.remove(timeExpiresToken);
    await preferences.remove(refreshToken);
    await preferences.remove(userID);
  }

  Future<bool> deleteUserType() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return await preferences.remove(userType);
  }

  Future<String?> getAccessToken() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return await preferences.getString(accesToken);
  }

  Future<String> getLanguage({String? defaultLanguage}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getString('localization') ?? defaultLanguage ?? 'en';
  }

  Future<bool?> getNotification() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getBool('notification');
  }

  Future<String?> getTimeExpiresAccessToken() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return await preferences.getString(timeExpiresToken);
  }

  Future<String?> getUserType() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return await preferences.getString(userType);
  }

  Future<bool> saveLanguage(String localeCode) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return await preferences.setString('localization', localeCode);
  }

  Future<bool> saveNotification(bool notification) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return await preferences.setBool('notification', notification);
  }

  Future<bool> saveToken({
    required String? accesTokenIn,
    required String? refreshTokenIn,
    required String? userIDIn,
    required String? timeExpiresAccesToken,
  }) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString(accesToken, accesTokenIn ?? '');
    await preferences.setString(timeExpiresToken, timeExpiresAccesToken ?? '');
    await preferences.setString(refreshToken, refreshTokenIn ?? '');
    return await preferences.setString(userID, userIDIn ?? '');
  }

  ///User
  Future<bool> saveUserType({required String userTypes}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return await preferences.setString(userType, userTypes);
  }
}
