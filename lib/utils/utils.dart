import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'add_null_number.dart';

class Utils {
  static bool get isAndroid {
    return Platform.isAndroid;
  }

  static bool get isDesktop {
    return Platform.isWindows || Platform.isMacOS || Platform.isLinux;
  }

  static bool get isDesktopTable {
    return isDesktop || isTablet;
  }

  static bool get isMac {
    return Platform.isMacOS;
  }

  static bool get isRealDesktop {
    return Platform.isWindows || Platform.isLinux;
  }

  static bool get isTablet {
    return _getDeviceType == 'tablet' && !isDesktop;
  }

  static String get _getDeviceType {
    // ignore: deprecated_member_use
    final data = MediaQueryData.fromView(WidgetsBinding.instance.window);
    return data.size.shortestSide < 550 ? 'phone' : 'tablet';
  }

  static asyncWithTimer(Future value, {String? name}) async {
    final time = DateTime.now().millisecondsSinceEpoch;
    Utils.printDebugMode("TIMER start init: ${name ?? value.toString()}");
    final val = await value;

    Utils.printDebugMode(
        "TIMER end init: ${name ?? value.toString()}, time = ${DateTime.now().millisecondsSinceEpoch - time}");

    return val;
  }

  static Future<bool> canLaunch(String scheme, String path) async {
    if (await canLaunchUrl(Uri(scheme: scheme, path: path))) {
      await launchUrl(Uri(scheme: scheme, path: path));
      return true;
    }
    return false;
  }

  static String dateFromTo(DateTime dateFrom, DateTime dateTo) {
    return "${AddNullNumber(dateFrom.day).toString()}.${AddNullNumber(dateFrom.month).toString()}.${dateFrom.year} - ${AddNullNumber(dateTo.day).toString()}.${AddNullNumber(dateTo.month).toString()}.${dateTo.year}";
  }

  static String getTextAvatar(String? nameCompany) {
    String _avatarText = '';
    if ((nameCompany?.runes.length ?? 0) >= 1) {
      _avatarText += String.fromCharCodes(nameCompany!.runes, 0, 2);
    }
    return _avatarText;
  }

  static Future<void> launchInBrowser(String path) async {
    if (!await launchUrl(Uri.parse(path))) {
      throw 'Could not launch $path)';
    }
  }

  static void printDebugMode(Object? object) {
    if (kDebugMode) {
      debugPrint(object.toString());
    }
  }

  static String? uriStringToString(String? uri) {
    return uri != null
        ? uri.replaceFirst('mxc://',
            'https://saon-matrixdevtest.page/_matrix/media/r0/download/')
        : null;
  }

  static String? uriToString(Uri? uri) {
    return uri != null
        ? uri.toString().replaceFirst('mxc://',
            'https://saon-matrixdevtest.page/_matrix/media/r0/download/')
        : null;
  }
}
