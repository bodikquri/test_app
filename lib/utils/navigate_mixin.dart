import 'package:flutter_modular/flutter_modular.dart';
import 'package:test_app/modules/routes_interface.dart';

class CustomStack<E> {
  final _list = <E>[];

  bool get isEmpty => _list.isEmpty;

  bool get isNotEmpty => _list.isNotEmpty;

  E get peek => _list.last;

  void clear() => _list.clear();

  bool haveThis(E obj) {
    if (isNotEmpty) {
      return _list.contains(obj);
    }
    return false;
  }

  E pop() => _list.removeLast();

  void popUntil(E value) {
    if (_list.isNotEmpty) {
      while (_list.last != value) {
        _list.removeLast();
        if (_list.isEmpty) return;
      }
    }
  }

  void push(E value) => _list.add(value);

  @override
  String toString() => _list.toString();
}

/*
 Щоб використовувати методи NavigateMixin, підключіть його до будь-якого класу за допомогою with.
 Приклад: class TestClass with NavigateMixin
*/
mixin NavigateMixin {
  CustomStack<String> get history => Modular.get<CustomStack<String>>();

  // Перевірка чи є поточний екран останнім у стеку екранів
  bool canPop() {
    return Modular.to.canPop();
  }

  // Закрити поточний екран із видаленням його зі стека
  Future<void> navigatePop({int count = 1}) async {
    for (var i = 0; i < count; i++) {
      if (canPop() && Modular.to.navigateHistory.length > 2) {
        Modular.to.pop();
      } else if (Modular.to.navigateHistory.length > 2) {
        String path = Modular
            .to.navigateHistory[Modular.to.navigateHistory.length - 2].name;
        Modular.to.navigateHistory.removeLast();
        Modular.to.navigateHistory.removeLast();
        Modular.to.navigateHistory;
        await Modular.to.pushNamed(
          path,
        );
      } else if (Modular.to.navigateHistory.length > 1) {
        Modular.to.pushReplacementNamed(Modular
            .to.navigateHistory[Modular.to.navigateHistory.length - 2].name);
      }
    }
  }

  // Відкрити новий екран із додаванням їх у загальний стек екранів.
  // Параметр route - роут для нового екрану, наприклад: navigateReplace(CaveRoute());
  Future<void> navigatePush(RouteDestination route) async {
    history.push(route.path());

    if (Modular.to.path != route.path()) {
      await Modular.to.pushNamed(
        route.path(),
        arguments: route.args(),
      );
    } else if (route.canDublicate) {
      await Modular.to.pushNamed(
        route.path(),
        arguments: route.args(),
      );
    }
  }

  Future<void> navigatePushWithRemoveLastRoute(RouteDestination route,
      {int indexRemoves = 1}) async {
    if (Modular.to.navigateHistory.length - indexRemoves < 2) {
      return;
    }
    if (Modular.to.navigateHistory.length > 2) {
      for (int i = 0; i < indexRemoves; i++) {
        Modular.to.navigateHistory.removeLast();
      }
      await Modular.to.pushNamed(
        route.path(),
      );
    }
  }

  // Відкрити новий екран із заміною поточного в стеку
  void navigateReplace(RouteDestination route) async {
    Modular.to.pushReplacementNamed(
      route.path(),
      arguments: route.args(),
    );
  }

  // Відкрити новий екран зі скиданням всіх екранів у стеку
  void navigateReplaceAll(RouteDestination route) async {
    history.clear();
    history.push(route.path());
    Modular.to.navigate(
      route.path(),
      arguments: route.args(),
    );
  }

  // Видалити з стека всі екрани, крім кореневого (початкового екрану)
  void navigateUntilRoot() => Modular.to.popUntil((p0) => p0.isFirst);
}
