class AddNullNumber {
  final int number;

  AddNullNumber(this.number);

  String toString() {
    return number < 10 ? '0$number' : number.toString();
  }
}
