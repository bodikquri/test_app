import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:test_app/resources/theme/app_theme.dart';

import 'common/flavors_utils.dart';
import 'generated/l10n.dart';
import 'utils/navigate_mixin.dart';

class App extends StatelessWidget with NavigateMixin {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      AppConfig.initAppConfig(
        packageName: packageInfo.packageName,
        appName: packageInfo.appName,
        appVersion: packageInfo.version,
        buildNumber: packageInfo.buildNumber,
      );
    });
    return ScreenUtilInit(
      rebuildFactor: RebuildFactors.sizeAndViewInsets,
      designSize: Size(390, 844),
      builder: (context, child) => GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: WillPopScope(
            onWillPop: () async {
              navigatePop();
              return false;
            },
            child: MaterialApp.router(
              routeInformationParser: Modular.routeInformationParser,
              routerDelegate: Modular.routerDelegate,
              debugShowCheckedModeBanner: false,
              localizationsDelegates: [
                S.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
              supportedLocales: S.delegate.supportedLocales,  
              theme: AppTheme.of(context).themeData,
              builder: (context, child) {
                return MediaQuery(
                  data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
                  child: SafeArea(child: child ?? Container()),
                );
              },
            ),
          )),
    );
  }
}
