import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

abstract class AppThemeColorScheme extends ColorScheme {
  static _LightColorScheme light = _LightColorScheme();
  static _DarkColorScheme dark = _DarkColorScheme();

  final LinearGradient linearGradient;
  final Color colorForLinearOne;
  final Color colorForLinearTwo;
  final Color dropDownTextColor;
  final Color defaultColorText;
  final Color textForMyLocation;
  final Color background;
  final Color blue;
  final Color white;
  final Color black;
  final Color grey100;
  final Color veryLightPink;
  final Color grey;
  final Color green;
  final Color lightGreen;
  final Color grey200;
  final Color brownGray;
  final Color black087;
  final Color firstGradiend;
  final Color secondGradient;
  final Color black054;
  final Color black038;
  final Color black016;
  final Color dark87;
  final Color light54;
  final Color light32;
  final Color black008;
  final Color light16;
  final Color lightWhite;
  final Color error;
  final Color errorContainer;
  final Color onError;
  final Color red;
  final Color transparent;
  final Color colorContainerOnNamedScreen;
  final Color labelTextAndDivider;
  @override
  final Brightness brightness;

  AppThemeColorScheme(
      {required this.dropDownTextColor,
      required this.colorForLinearOne,
      required this.colorForLinearTwo,
      required this.defaultColorText,
      required this.textForMyLocation,
      required this.linearGradient,
      required this.background,
      required this.green,
      required this.lightGreen,
      required this.black087,
      required this.firstGradiend,
      required this.secondGradient,
      required this.black054,
      required this.black038,
      required this.black016,
      required this.dark87,
      required this.light54,
      required this.black008,
      required this.light32,
      required this.light16,
      required this.lightWhite,
      required this.brightness,
      required this.blue,
      required this.white,
      required this.black,
      required this.grey,
      required this.grey100,
      required this.grey200,
      required this.veryLightPink,
      required this.brownGray,
      required this.error,
      required this.errorContainer,
      required this.onError,
      required this.labelTextAndDivider,
      required this.red,
      required this.colorContainerOnNamedScreen})
      : this.transparent = Colors.transparent,
        super(
          brightness: brightness,
          primary: light54,
          secondary: black,
          surface: blue,
          background: lightWhite,
          error: error,
          errorContainer: errorContainer,
          onPrimary: Colors.black,
          onSecondary: Colors.black,
          onSurface: Colors.white,
          onBackground: black,
          onError: onError,
        );
}

class _DarkColorScheme extends AppThemeColorScheme {
  _DarkColorScheme()

      : super(
            colorForLinearTwo: Color(0xffFA930C),
            colorForLinearOne: Color(0xffC7750A),
            labelTextAndDivider: Color(0xffC9C9C9),
            colorContainerOnNamedScreen: Color(0xff2E2E38),
            dropDownTextColor: Colors.black,
            defaultColorText: Color(0xffFFFFFF),
            textForMyLocation: Color(0xffE1850B),
            linearGradient: LinearGradient(
              colors: <Color>[Color(0xff7750A), Color(0xffFA930C)],
            ),
            light32: HexColor("#D9DCFF"),
            background: Color(0xff1F1F21),
            green: HexColor("#00DCBA"),
            lightGreen: HexColor("#D4F7F0"),
            firstGradiend: HexColor("#01004A"),
            secondGradient: HexColor("#1E3A86"),
            black087: HexColor("#000000").withOpacity(0.87),
            black054: HexColor("#000000").withOpacity(0.54),
            black038: HexColor("#000000").withOpacity(0.38),
            black016: HexColor("#000000").withOpacity(0.16),
            black008: HexColor("#000000").withOpacity(0.08),
            dark87: HexColor("#01004A"),
            light54: HexColor("#B8BFFF"),
            light16: HexColor("#EEF0FF"),
            lightWhite: HexColor("#fbfbff"),
            brightness: Brightness.dark,
            blue: HexColor('#0070FF'),
            white: HexColor('#ffffff'),
            black: HexColor('#000000'),
            grey: HexColor('#AAAAAA'),
            grey100: HexColor('#666666'),
            grey200: HexColor('#44413e'),
            veryLightPink: HexColor('#dddddd'),
            brownGray: HexColor('#999999'),
            error: HexColor("#fbfbff"),
            errorContainer: HexColor("#B00020"),
            onError: HexColor("#B00020"),
            red: HexColor("#B00020"));
}

class _LightColorScheme extends AppThemeColorScheme {
  _LightColorScheme()
      : super(
            colorForLinearTwo: Color(0xffFA930C),
            colorForLinearOne: Color(0xffC7750A),
            labelTextAndDivider: Color(0xffC9C9C9),
            colorContainerOnNamedScreen: Color(0xff2E2E38),
            dropDownTextColor: Colors.black,
            defaultColorText: Color(0xffFFFFFF),
            textForMyLocation: Color(0xffE1850B),
            linearGradient: LinearGradient(
              colors: <Color>[
                Color(0xffC7750A).withOpacity(0.9),
                Color(0xffFA930C)
              ],
            ),
            background: Color(0xff1F1F21),
            light32: HexColor("#D9DCFF"),
            green: HexColor("#00DCBA"),
            lightGreen: HexColor("#D4F7F0"),
            firstGradiend: HexColor("#01004A"),
            secondGradient: HexColor("#1E3A86"),
            black087: HexColor("#000000").withOpacity(0.87),
            black054: HexColor("#000000").withOpacity(0.54),
            black038: HexColor("#000000").withOpacity(0.38),
            black016: HexColor("#000000").withOpacity(0.16),
            black008: HexColor("#000000").withOpacity(0.08),
            dark87: HexColor("#01004A"),
            light54: HexColor("#B8BFFF"),
            light16: HexColor("#EEF0FF"),
            lightWhite: HexColor("#fbfbff"),
            brightness: Brightness.dark,
            blue: HexColor('#0070FF'),
            white: HexColor('#ffffff'),
            black: HexColor('#000000'),
            grey: HexColor('#AAAAAA'),
            grey100: HexColor('#666666'),
            grey200: HexColor('#44413e'),
            veryLightPink: HexColor('#dddddd'),
            brownGray: HexColor('#999999'),
            error: HexColor("#fbfbff"),
            errorContainer: HexColor("#B00020"),
            onError: HexColor("#B00020"),
            red: HexColor("#B00020"));
}
