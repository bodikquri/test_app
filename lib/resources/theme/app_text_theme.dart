import 'dart:ui' as ui show Shadow, FontFeature;

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_app/gen/fonts.gen.dart';

import 'app_theme_color_scheme.dart';

class AppTextTheme extends TextTheme {
  final AppThemeTextStyle headLine4Bold;
  final AppThemeTextStyle headLine5Bold;
  final AppThemeTextStyle headLine6;
  final AppThemeTextStyle body2;
  final AppThemeTextStyle textInContainerLastScreen;
  final AppThemeTextStyle body1;
  final AppThemeTextStyle myLocation;
  final AppThemeTextStyle buttonInContainer;
  final AppThemeTextStyle subtitle1;
  final AppThemeTextStyle overline;
  final AppThemeTextStyle caption;
  final AppThemeTextStyle descriptionInLogScreen;
  final AppThemeTextStyle textForNamedScreen;
  final AppThemeTextStyle containerText;
  final AppThemeTextStyle styleBeetweenContainer;
  final AppThemeTextStyle amountAndAnotherText;
  

  const AppTextTheme({
    required this.textForNamedScreen,
    required this.descriptionInLogScreen,
    required this.headLine4Bold,
    required this.headLine5Bold,
    required this.headLine6,
    required this.body2,
    required this.textInContainerLastScreen,
    required this.body1,
    required this.myLocation,
    required this.buttonInContainer,
    required this.subtitle1,
    required this.overline,
    required this.caption,
    required this.containerText,
    required this.styleBeetweenContainer,
    required this.amountAndAnotherText,
  }) : super();

  factory AppTextTheme.byColorScheme(
    AppThemeColorScheme colorScheme,
  ) =>
      AppTextTheme(
          body1: AppThemeTextStyle(
            colorScheme,
            fontSize: 12.sp,
            // color: colorScheme.black087,
            fontStyle: FontStyle.normal,
            fontFamily: MyFontFamily.robotoRegular,
            fontWeight: FontWeight.w300,
            height: 1.142,
          ),
          amountAndAnotherText: AppThemeTextStyle(
            colorScheme,
            fontSize: 16.sp,
            color: colorScheme.black087,
            fontStyle: FontStyle.normal,
            fontFamily: MyFontFamily.robotoMedium,
            fontWeight: FontWeight.w400,
            height: 1.142,
          ),
          myLocation: AppThemeTextStyle(
            colorScheme,
            fontSize: 16.sp,
            color: colorScheme.black087,
            fontStyle: FontStyle.normal,
            fontFamily: MyFontFamily.robotoMedium,
            fontWeight: FontWeight.w400,
            height: 1.142,
          ),
          body2: AppThemeTextStyle(
            colorScheme,
            fontSize: 14.sp,
            letterSpacing: 0.2,
            color: colorScheme.black054,
            fontStyle: FontStyle.normal,
            fontFamily: MyFontFamily.robotoRegular,
            fontWeight: FontWeight.w400,
            height: 1.142,
          ),
          textInContainerLastScreen: AppThemeTextStyle(
            colorScheme,
            fontSize: 14.sp,
            letterSpacing: 0.25,
            color: colorScheme.black087,
            fontStyle: FontStyle.normal,
            fontFamily: MyFontFamily.robotoMedium,
            fontWeight: FontWeight.w400,
            height: 1.142,
          ),
          headLine5Bold: AppThemeTextStyle(
            colorScheme,
            fontSize: 24.sp,
            letterSpacing: 0.18,
            color: colorScheme.black087,
            fontStyle: FontStyle.normal,
            fontFamily: MyFontFamily.robotoMedium,
            fontWeight: FontWeight.w500,
            height: 1.142,
          ),
          headLine6: AppThemeTextStyle(
            colorScheme,
            fontSize: 20.sp,
            letterSpacing: 0.15,
            color: colorScheme.dark87,
            fontStyle: FontStyle.normal,
            fontFamily: MyFontFamily.robotoMedium,
            fontWeight: FontWeight.w500,
            height: 1.142,
          ),
          headLine4Bold: AppThemeTextStyle(
            colorScheme,
            fontSize: 28.sp,
            color: colorScheme.white,
            fontStyle: FontStyle.normal,
            fontFamily: MyFontFamily.robotoMedium,
            fontWeight: FontWeight.w600,
            height: 1.142,
          ),
          subtitle1: AppThemeTextStyle(
            colorScheme,
            fontSize: 16.sp,
            letterSpacing: 0.15,
            color: colorScheme.dark87,
            fontStyle: FontStyle.normal,
            fontFamily: MyFontFamily.robotoRegular,
            fontWeight: FontWeight.w400,
            height: 1.142,
          ),
          containerText: AppThemeTextStyle(
            colorScheme,
            fontSize: 18.sp,
            color: colorScheme.blue,
            fontStyle: FontStyle.normal,
            fontFamily: MyFontFamily.robotoMedium,
            fontWeight: FontWeight.w500,
            height: 1.142,
          ),
          buttonInContainer: AppThemeTextStyle(
            colorScheme,
            letterSpacing: 0.9,
            fontSize: 18.sp,
            color: colorScheme.blue,
            fontStyle: FontStyle.normal,
            fontFamily: MyFontFamily.robotoMedium,
            fontWeight: FontWeight.w700,
            height: 1.142,
          ),
          overline: AppThemeTextStyle(
            colorScheme,
            fontSize: 10.sp,
            letterSpacing: 1,
            color: colorScheme.black054,
            fontStyle: FontStyle.normal,
            fontFamily: MyFontFamily.robotoMedium,
            fontWeight: FontWeight.w500,
            height: 1.142,
          ),
          caption: AppThemeTextStyle(
            colorScheme,
            fontSize: 18.sp,
            letterSpacing: 0.4,
            color: colorScheme.black054,
            fontStyle: FontStyle.normal,
            fontFamily: MyFontFamily.robotoRegular,
            fontWeight: FontWeight.w400,
            height: 1.142,
          ),
          descriptionInLogScreen: AppThemeTextStyle(
            colorScheme,
            fontSize: 28.sp,
            letterSpacing: 0.4,
            color: colorScheme.black054,
            fontStyle: FontStyle.normal,
            fontFamily: MyFontFamily.robotoRegular,
            fontWeight: FontWeight.w600,
            height: 1.142,
          ),
          textForNamedScreen: AppThemeTextStyle(
            colorScheme,
            fontSize: 14.sp,
            color: colorScheme.black054,
            fontStyle: FontStyle.normal,
            fontFamily: MyFontFamily.robotoRegular,
            fontWeight: FontWeight.w400,
            height: 1.142,
          ),
          styleBeetweenContainer: AppThemeTextStyle(
            colorScheme,
            fontSize: 16.sp,
            color: colorScheme.black054,
            fontStyle: FontStyle.normal,
            fontFamily: MyFontFamily.robotoRegular,
            fontWeight: FontWeight.w600,
            height: 1.142,
          ));
}

class AppThemeTextStyle extends TextStyle {
  final AppThemeColorScheme colorScheme;

  const AppThemeTextStyle(
    this.colorScheme, {
    bool inherit = true,
    Color? color,
    Color? backgroundColor,
    String? fontFamily,
    List<String>? fontFamilyFallback,
    double? fontSize,
    FontWeight? fontWeight,
    FontStyle? fontStyle,
    double? letterSpacing,
    double? wordSpacing,
    TextBaseline? textBaseline,
    double? height,
    Locale? locale,
    Paint? foreground,
    Paint? background,
    TextDecoration? decoration,
    Color? decorationColor,
    TextDecorationStyle? decorationStyle,
    double? decorationThickness,
    String? debugLabel,
    List<ui.Shadow>? shadows,
    List<ui.FontFeature>? fontFeatures,
  }) : super(
          inherit: inherit,
          color: color,
          backgroundColor: backgroundColor,
          fontFamily: fontFamily ?? MyFontFamily.robotoRegular,
          fontFamilyFallback: fontFamilyFallback,
          fontSize: fontSize,
          fontWeight: fontWeight,
          fontStyle: fontStyle,
          letterSpacing: letterSpacing,
          wordSpacing: wordSpacing,
          textBaseline: textBaseline,
          height: height,
          locale: locale,
          foreground: foreground,
          background: background,
          decoration: decoration,
          decorationColor: decorationColor,
          decorationStyle: decorationStyle,
          decorationThickness: decorationThickness,
          debugLabel: debugLabel,
          shadows: shadows,
          fontFeatures: fontFeatures,
        );
}
