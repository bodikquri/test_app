import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_app/resources/theme/app_theme.dart';

class InputField extends StatefulWidget {
  final TextEditingController controller;

  final String? labelText;
  final Function(String value)? onChanged;
  final String? errorText;
  final String? hintText;
  final bool obscureText;
  final bool autofocus;
  final TextInputType? textInputType;
  final TextAlign? textAlign;
  final TextStyle? inputTextStyle;
  final TextStyle? hintTextStyle;
  final bool enableInteractiveSelection;
  final bool readOnly;
  final FocusNode? focusNode;
  final TextCapitalization? textCapitalization;
  final int? maxLength;
  final List<TextInputFormatter>? inputFormatters;
  final GestureTapCallback? onTap;
  final EdgeInsetsGeometry? contentPadding;
  final bool isDense;
  final String? suffixText;
  final String? prefixText;
  const InputField({
    required this.controller,
    this.labelText,
    this.onChanged,
    this.errorText,
    this.hintText,
    this.obscureText = false,
    this.autofocus = false,
    this.textInputType,
    this.textAlign,
    this.inputTextStyle,
    this.hintTextStyle,
    this.enableInteractiveSelection = true,
    this.readOnly = false,
    this.focusNode,
    this.textCapitalization,
    this.maxLength,
    this.inputFormatters,
    this.onTap,
    this.contentPadding,
    this.isDense = false,
    this.suffixText,
    this.prefixText,
  });

  @override
  State<InputField> createState() => _InputFieldState();
}

class _InputFieldState extends State<InputField> {
  bool isFilled = false;

  @override
  Widget build(BuildContext context) {
    final colors = AppTheme.of(context).colors;
    final textTheme = AppTheme.of(context).textTheme;

    return TextField(
      style: (widget.inputTextStyle ?? textTheme.body2).copyWith(
        color: colors.black087,
      ),
      controller: widget.controller,
      focusNode: widget.focusNode,
      scrollPadding: EdgeInsets.symmetric(vertical: 10.h),
      onChanged: (String value) {
        if (widget.onChanged != null) {
          widget.onChanged!(value);
        }
      },
      autofocus: widget.autofocus,
      obscureText: widget.obscureText,
      textAlign: widget.textAlign ?? TextAlign.start,
      obscuringCharacter: "•",
      cursorColor: colors.primary,
      cursorWidth: 1.w,
      keyboardType: widget.textInputType,
      textCapitalization: widget.textCapitalization ?? TextCapitalization.none,
      enableInteractiveSelection: widget.enableInteractiveSelection,
      readOnly: widget.readOnly,
      maxLength: widget.maxLength,
      inputFormatters: widget.inputFormatters ?? null,
      onTap: widget.onTap,
      decoration: InputDecoration(
        prefixText: widget.prefixText,
        suffixText: widget.suffixText,
        suffixStyle: textTheme.body2.copyWith(color: colors.blue),
        contentPadding: widget.contentPadding ??
            EdgeInsets.symmetric(
              horizontal: 16.w,
              vertical: 14.h,
            ),
        isDense: widget.isDense,
        labelText: widget.labelText,
        errorText: widget.errorText,
        errorStyle: textTheme.body2.copyWith(
          color: colors.error,
          height: 1,
        ),
        hintText: widget.hintText ?? widget.labelText,
        hintStyle: widget.hintTextStyle ??
            textTheme.body2.copyWith(color: colors.white.withOpacity(0.4)),
        alignLabelWithHint: true,
        errorMaxLines: 2,
        floatingLabelBehavior: FloatingLabelBehavior.never,
        labelStyle: textTheme.body2.copyWith(
          color: colors.white,
        ),
        fillColor: colors.white,
        filled: true,
        border: OutlineInputBorder(
          borderSide: BorderSide(
            color: isFilled ? colors.white : colors.white,
            width: 1.5.w,
          ),
          borderRadius: BorderRadius.zero,
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: colors.primary,
            width: 1.5.w,
          ),
          borderRadius: BorderRadius.zero,
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: isFilled ? colors.white : colors.error,
            width: 1.5.w,
          ),
          borderRadius: BorderRadius.zero,
        ),
        errorBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: colors.error,
            width: 1.5.w,
          ),
          borderRadius: BorderRadius.zero,
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: colors.error,
            width: 1.5.w,
          ),
          borderRadius: BorderRadius.zero,
        ),
      ),
    );
  }

  @override
  void initState() {
    widget.controller.addListener(() {
      setState(() {
        isFilled = widget.controller.text.isNotEmpty;
      });
    });
    super.initState();
  }
}
