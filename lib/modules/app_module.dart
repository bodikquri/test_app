import 'package:flutter/cupertino.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_app/modules/home/home_module.dart';
import 'package:test_app/modules/home/home_routes_config.dart';
import 'package:test_app/modules/login/login_module.dart';
import 'package:test_app/modules/login/login_routes_config.dart';
import 'package:test_app/modules/splash/splash_cubit.dart';
import 'package:test_app/modules/splash/splash_screen.dart';
import 'package:test_app/utils/navigate_mixin.dart';

class AppModule extends Module {
  @override
  List<Bind> get binds => [
        Bind.factory(
          (i) => TextEditingController(),
        ),
        Bind.singleton(
          (i) => CustomStack<String>(),
        ),
        Bind(
          (_) => Logger(
            printer: PrettyPrinter(),
          ),
        ),
        AsyncBind<SharedPreferences>((i) => SharedPreferences.getInstance()),
        Bind((i) => SplashCubit()),
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute(
          '/',
          child: (_, __) => SplashScreen(),
        ),
        ModuleRoute(
          LoginRoutesConfig.moduleRoute,
          module: LoginModule(),
        ),
        ModuleRoute(HomeRoutesConfig.moduleRoute, module: HomeModule())
      ];
}
