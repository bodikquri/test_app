import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_app/modules/splash/splash_cubit.dart';
import 'package:test_app/resources/theme/app_theme.dart';
import 'package:test_app/utils/navigate_mixin.dart';

import '../../gen/assets.gen.dart';
import '../home/home_routes_config.dart';
import '../login/login_routes_config.dart';

class SplashScreen extends StatelessWidget with NavigateMixin {
  SplashScreen({Key? key}) : super(key: key);

  SplashCubit get cubit => Modular.get<SplashCubit>();

  @override
  Widget build(BuildContext context) {
    final colors = AppTheme.of(context).colors;
    return BlocConsumer<SplashCubit, SplashState>(
        bloc: cubit,
        listener: (context, state) {
          if (state is SplashFirstLunchState) {
            navigateReplaceAll(LoginRoute());
          }
          if (state is SplashSuccessState) {
            navigateReplaceAll(HomeRoute());
          }
        },
        builder: (context, state) {
          return Scaffold(
            backgroundColor: colors.background,
            body: Container(
         
              child: Stack(children: [
                Container(
                ),
                Container(
                  margin: EdgeInsets.only(left: 170.w, top: 10.h),
                  child: MyAssets.images.svg.lineThree.svg(
                    height: 387.h,
                    width: 304.w,
                  ),
                ),
                FittedBox(
                  child: Container(
                    margin: EdgeInsets.only(
                        left: 45.w, right: 42.w, top: 271.h, bottom: 454.h),
                    child: MyAssets.images.svg.splashTruck.svg(
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    left: 135.w,
                    top: 91.h,
                  ),
                  child: MyAssets.images.svg.lineOnes
                      .svg(
                    width: 311.w,
                    height: 625.h,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: 239.h,
                  ),
                  child: MyAssets.images.svg.lineSecond
                      .svg(
                    width: 205.91.w,
                    height: 598.h,
                  ),
                ),
              ]),
            ),
          );
        });
  }
}
