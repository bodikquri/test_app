part of 'splash_cubit.dart';

class SplashFirstLunchState extends SplashState {}

class SplashInitialState extends SplashState {}

class SplashLoadingState extends SplashState {}

abstract class SplashState extends Equatable {
  const SplashState();

  @override
  List<Object> get props => [DateTime.now().toIso8601String()];
}

class SplashSuccessState extends SplashState {}
