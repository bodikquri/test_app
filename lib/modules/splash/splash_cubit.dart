import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:test_app/modules/app_module.dart';
import 'package:test_app/modules/splash/shard_storage.dart';

part 'splash_state.dart';

class SplashCubit extends Cubit<SplashState> {
  SplashCubit() : super(SplashInitialState()) {
    Future.delayed(Duration(milliseconds: 2500), () async {
      await Modular.isModuleReady<AppModule>();
      if (ShardStorage().getBool() == true)
        emit(SplashSuccessState());
      else
        emit(SplashFirstLunchState());
    });
  }
}
