import 'package:flutter_modular/flutter_modular.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ShardStorage {
  static const String wasSeeTutorial = "bool";
  SharedPreferences get _storage => Modular.get<SharedPreferences>();

  bool? getBool() {
    // final storage = await _storage;
    return _storage.getBool(wasSeeTutorial);
  }

  Future<void> setBool([bool value = true]) async {
    // final storage = await _storage;
    await _storage.setBool(wasSeeTutorial, value);
  }
}
