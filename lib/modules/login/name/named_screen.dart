import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_app/gen/assets.gen.dart';
import 'package:test_app/generated/l10n.dart';
import 'package:test_app/modules/login/widgets/widget_for_name_screen.dart';
import 'package:test_app/resources/theme/app_theme.dart';
import 'package:test_app/utils/navigate_mixin.dart';

import '../cubit/named/named_cubit.dart';
import '../myLocationscreen/my_location.screen.dart';
import '../screens/login_screen.dart';
import '../widgets/container_for_screens.dart';
import '../widgets/text_for_named_screen.dart';

List<String> list = <String>['One', 'Two', 'Three', 'Four', 'jdjdjd', 'ffhf'];

class NamedScreen extends StatefulWidget with NavigateMixin {
  NamedScreen({Key? key}) : super(key: key);

  @override
  State<NamedScreen> createState() => _NamedScreenState();
}

class _NamedScreenState extends State<NamedScreen> {
  String dropdownValue = list.first;
  NamedCubit get cubit => Modular.get<NamedCubit>();

  @override
  Widget build(BuildContext context) {
    final colors = AppTheme.of(context).colors;
    final textTheme = AppTheme.of(context).textTheme;
    StreamBuilder<User?>(
        stream: FirebaseAuth.instance.authStateChanges(),
        builder: (BuildContext context, AsyncSnapshot snaphot) {
          if (snaphot.hasError) {
            return Text(snaphot.error.toString());
          }
          if (snaphot.connectionState == ConnectionState.active) {
            if (snaphot.data == null) {
              return LoginScreen();
            } else {
              return NamedScreen();
            }
          }
          return Center();
        });
    return Scaffold(
      backgroundColor: colors.background,
      body: Stack(children: [
        Container(
          child: MyAssets.images.svg.lineForNamed.svg(),
        ),
        Container(
          child: MyAssets.images.svg.lineThree.svg(),
        ),
        Column(
          children: [
            Container(
                margin: EdgeInsets.only(top: 254.h),
                child:
                    TextForNamedScreen(text: S.current.firstNameNamedScreen)),
            SizedBox(
              height: 5.h,
            ),
            WidgetForNamedScreen(text: S.current.firstNameNamedScreen),
            SizedBox(
              height: 19.h,
            ),
            TextForNamedScreen(text: S.current.lastNameNamedScreen),
            SizedBox(
              height: 5.h,
            ),
            WidgetForNamedScreen(text: S.current.lastNameNamedScreen),
            SizedBox(
              height: 19.h,
            ),
            TextForNamedScreen(text: S.current.truckTypeNamedScreen),
            SizedBox(
              height: 5.h,
            ),
            Container(
                margin: EdgeInsets.only(left: 20.w, right: 20.w),
                height: 60.h,
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: colors.black.withOpacity(0.5),
                        blurRadius: 4,
                        offset: Offset(2, 2),
                      ),
                    ],
                    color: colors.colorContainerOnNamedScreen,
                    borderRadius: BorderRadius.circular(10.sp)),
                padding: EdgeInsets.only(left: 10.w, right: 20.w),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                    isExpanded: true,
                    value: dropdownValue,
                    icon: MyAssets.images.svg.iconForNamedScreen.svg(),
                    elevation: 16,
                    style: textTheme.body1
                        .copyWith(color: colors.labelTextAndDivider),
                    underline: Container(
                      height: 2.h,
                    ),
                    onChanged: (String? value) {
                      setState(() {
                        dropdownValue = value!;
                      });
                    },
                    items: list.map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                )),
            SizedBox(
              height: 40.h,
            ),
            InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => LocationScreen()));
                },
                child: WidgetContainer(
                  text: S.current.textForContainer,
                )),
          ],
        )
      ]),
    );
  }
}
