import 'package:equatable/equatable.dart';

class AlertDialogState extends LocationState {}

class InitialState extends LocationState {}

class LaterLoginState extends LocationState {}

class LoadingState extends LocationState {}

abstract class LocationState extends Equatable {
  const LocationState();

  @override
  List<Object> get props => [DateTime.now().toIso8601String()];
}

class LoginEmailState extends LocationState {}

class LoginFacebookState extends LocationState {}

class LoginGoogleState extends LocationState {}

class OpenState extends LocationState {}

class SuccessState extends LocationState {}
