import 'dart:async';

import 'package:bloc/bloc.dart';

import 'mylocation_state.dart';

class LocationCubit extends Cubit<LocationState> {
  Map<String, StreamSubscription> subscriptions = {};

  final name = "lol";

  LocationCubit() : super(InitialState());

  @override
  Future<void> close() {
    _cancelSubscriptions();
    return super.close();
  }

  loginEmail() {
    emit(LoginEmailState());
  }

  loginFacebook() {
    emit(LoginFacebookState());
  }

  loginGoogle() {
    emit(LoginGoogleState());
  }

  void loginLater() {
    emit(LaterLoginState());
  }

  void ontap() {}

  void _cancelSubscriptions() {
    subscriptions.forEach((key, value) {
      value.cancel();
    });
  }
}
