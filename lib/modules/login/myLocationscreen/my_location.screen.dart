import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:test_app/generated/l10n.dart';
import 'package:test_app/last_map_screen/lastmap_screen.dart';

import '../../../gen/assets.gen.dart';
import '../../../resources/theme/app_theme.dart';
import '../widgets/container_for_screens.dart';
import '../widgets/cupertino_picker_widget.dart';


class LocationScreen extends StatefulWidget {
  const LocationScreen({super.key});

  @override
  State<LocationScreen> createState() => _LocationScreenState();
}

class _LocationScreenState extends State<LocationScreen> {
  Position? _currentLocation;
  late bool servicePermission = false;

  late LocationPermission permission;
  String _currentAdress = "";
  var item = 0;

  @override
  Widget build(BuildContext context) {
    final color = AppTheme.of(context).colors;
    final textTheme = AppTheme.of(context).textTheme;
    return Scaffold(
        backgroundColor: color.background,
        body: Stack(children: [
          Container(
            alignment: Alignment.centerRight,
            child: MyAssets.images.svg.lineThree.svg(),
          ),
          Container(
            margin: EdgeInsets.only(top: 50.h),
            alignment: Alignment.topRight,
            child: MyAssets.images.svg.lineOnes.svg(),
          ),
          Container(
            child: MyAssets.images.svg.lineForLocScreen.svg(),
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: MyAssets.images.svg.lineForLocScreenTwo.svg(),
          ),
          Column(children: [
            InkWell(
              onTap: () async {
                _currentLocation = await _getCurrentLocation();
                await _getAdressFromCoordinates();
                print("${_currentLocation}");
                print("${_currentAdress}");
              },
              child: Container(
                margin: EdgeInsets.only(top: 264.h),
                alignment: Alignment.center,
                child: MyAssets.images.svg.myLocation.svg(),
              ),
            ),
            SizedBox(
              height: 10.h,
            ),
            Text(S.current.myLocation,
                style: textTheme.myLocation
                    .copyWith(color: color.defaultColorText)),
            SizedBox(
              height: 10.h,
            ),
            Text('${_currentAdress}',
                style: TextStyle(color: color.textForMyLocation)),
            SizedBox(
              height: 40.h,
            ),
            Container(
              child: Text(S.current.cargoPrice,
                  style: textTheme.myLocation
                      .copyWith(color: color.defaultColorText)),
            ),
            SizedBox(
              height: 15.h,
            ),
            CupertinoPickerForMyLocationScreen(),
            SizedBox(
              height: 40.h,
            ),
            InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => LastMapScreen()));
                },
                child: WidgetContainer(
                  text: S.current.textForContainer,
                )),
          ]),
        ]));
  }

  _getAdressFromCoordinates() async {
    try {
      List<Placemark> placeMarks = await placemarkFromCoordinates(
          _currentLocation!.latitude, _currentLocation!.longitude);
      Placemark place = placeMarks[0];
      setState(() {
        _currentAdress = "${place.locality}, ${place.country}";
      });
    } catch (e) {
      print(e);
    }
  }

  Future<Position> _getCurrentLocation() async {
    servicePermission = await Geolocator.isLocationServiceEnabled();
    if (!servicePermission) {
      permission = await Geolocator.checkPermission();
      if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();
      }
    }

    return await Geolocator.getCurrentPosition();
  }
}
