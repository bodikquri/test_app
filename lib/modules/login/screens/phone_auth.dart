import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../generated/l10n.dart';
import '../../../resources/theme/app_theme.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  FirebaseAuth _auth = FirebaseAuth.instance;
  String _verificationId = '';

  @override
  Widget build(BuildContext context) {
    final colors = AppTheme.of(context).colors;
    final textTheme = AppTheme.of(context).textTheme;
    return Scaffold(
      backgroundColor: colors.background,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(S.current.phoneNumber),
            SizedBox(height: 8.h),
            TextFormField(
              keyboardType: TextInputType.phone,
              onChanged: (value) {},
            ),
            SizedBox(height: 16.h),
            ElevatedButton(
              onPressed: () {
                _sendVerificationCode(S.current.sendVerificationCode);
              },
              child: Text(S.current.sendCode),
            ),
            SizedBox(height: 16.h),
            TextFormField(
              keyboardType: TextInputType.number,
              onChanged: (value) {},
            ),
            SizedBox(height: 16.h),
            ElevatedButton(
              onPressed: () {
                _verifyCode(S.current.verifyCode);
              },
              child: Text(S.current.confirmCode),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _sendVerificationCode(String phoneNumber) async {
    PhoneVerificationCompleted verificationCompleted =
        (PhoneAuthCredential credential) async {
      await _auth.signInWithCredential(credential);
    };

    PhoneVerificationFailed verificationFailed =
        (FirebaseAuthException authException) {
      print(' ${authException.message}');
    };

    PhoneCodeSent codeSent =
        (String verificationId, [int? forceResendingToken]) {
      setState(() {
        _verificationId = verificationId;
      });
    };

    PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
        (String verificationId) {
      setState(() {
        _verificationId = verificationId;
      });
    };

    await _auth.verifyPhoneNumber(
      phoneNumber: phoneNumber,
      verificationCompleted: verificationCompleted,
      verificationFailed: verificationFailed,
      codeSent: codeSent,
      codeAutoRetrievalTimeout: codeAutoRetrievalTimeout,
    );
  }

  void _verifyCode(String code) async {
    PhoneAuthCredential credential = PhoneAuthProvider.credential(
      verificationId: _verificationId,
      smsCode: code,
    );
    await _auth.signInWithCredential(credential);
  }
}
