import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:test_app/generated/l10n.dart';
import 'package:test_app/modules/home/home_routes_config.dart';
import 'package:test_app/modules/login/cubit/login/login_cubit.dart';
import 'package:test_app/modules/login/screens/phone_auth.dart';
import 'package:test_app/resources/theme/app_theme.dart';
import 'package:test_app/utils/navigate_mixin.dart';

import '../../../gen/assets.gen.dart';
import '../../../main.dart';
import '../name/named_screen.dart';
import '../widgets/container_for_screens.dart';
import '../widgets/login_screen_widget_container.dart';

signInWithGoogle() async {
  try {
    GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();
    GoogleSignInAuthentication? googleAuth = await googleUser?.authentication;
    AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleAuth?.accessToken, idToken: googleAuth?.idToken);
    UserCredential userCredential =
        await FirebaseAuth.instance.signInWithCredential(credential);
    print(userCredential.user?.displayName);
    print(await userCredential.user?.getIdToken());
  } catch (e) {
    logger.e(e);
  }
}

class LoginScreen extends StatelessWidget with NavigateMixin {
  LoginScreen({Key? key}) : super(key: key);
  LoginCubit get cubit => Modular.get<LoginCubit>();

  @override
  Widget build(BuildContext context) {
    Future<void> _handleToken(String token) async {
      if (token != null) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => NamedScreen()));
      } else
        (error) {
          print("$error");
        };
    }

    GoogleSignIn _googleSignIn = GoogleSignIn(scopes: ['email']);

    Future<void> _handleSignOut() async {
      try {
        await _googleSignIn.signOut();
      } catch (error) {
        print("$error");
      }
    }

    final colors = AppTheme.of(context).colors;
    final textTheme = AppTheme.of(context).textTheme;
    return BlocConsumer<LoginCubit, LoginState>(
        bloc: cubit,
        listener: (context, state) async {
          if (state is AlertDialogState) {}
          if (state is LaterLoginState) {
            navigatePush(DashBoardRoute());
          }
        },
        builder: (context, state) {
          return Scaffold(
            backgroundColor: colors.background,
            body: Stack(children: [
              Container(
                margin: EdgeInsets.only(top: 280.h, right: 260.w),
                child: MyAssets.images.svg.lineSecond.svg(),
              ),
              Container(
                width: 304.w,
                height: 387.h,
                margin: EdgeInsets.only(left: 211.w, top: 243.h),
                child: MyAssets.images.svg.lineThree.svg(),
              ),
              Container(
                margin: EdgeInsets.only(
                  left: 90.w,
                ),
                child: MyAssets.images.svg.lineLogScreen.svg(),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 157.h,
                ),
                child: MyAssets.images.svg.shadowLogScreen.svg(),
              ),
              Container(
                margin: EdgeInsets.only(top: 238.h, left: 56.w, right: 56.w),
                child: MyAssets.images.svg.truck
                    .svg(
                  width: 278.w,
                ),
              ),
              Column(children: [
                Container(
                  margin: EdgeInsets.only(top: 102.h),
                  child: Text(
                    S.current.description,
                    style: textTheme.descriptionInLogScreen
                        .copyWith(color: colors.defaultColorText),
                  ),
                ),
                InkWell(
                    onTap: () async {
                      await signInWithGoogle();
                      if (IdTokenResult != null) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    NamedScreen()));
                      } else {
                        return signInWithGoogle();
                      }
                    },
                    child: WidgeContainerLoginScreen(
                      text: S.current.textForContainerWithIcon,
                      height: 508.h,
                      image: MyAssets.images.svg.icGoogle
                          .svg(color: colors.defaultColorText),
                    )),
                SizedBox(
                  height: 15.h,
                ),
                Container(
                  child: Text(S.current.textBetweenTwoContainer,
                      style: textTheme.styleBeetweenContainer
                          .copyWith(color: colors.defaultColorText)),
                ),
                SizedBox(
                  height: 15.h,
                ),
                InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) => LoginPage()));
                  },
                  child: WidgeContainerLoginScreen(
                    text: S.current.textForContainerWithIconSecond,
                    image: MyAssets.images.svg.phone.svg(),
                    height: 0.h,
                  ),
                ),
                SizedBox(
                  height: 4.5.h,
                ),
                InkWell(
                    onTap: _handleSignOut,
                    child: WidgetContainer(text: S.current.textForSignOut))
              ]),
            ]),
          );
        });
  }
}
