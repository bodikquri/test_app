import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../resources/theme/app_theme.dart';

class LastScreenContainers extends StatelessWidget {
  final Color color;
  final String text;
  Color radiusButtonColor = Colors.blue;

  Gradient? containerGradient;

  double zoomValue = 12.0;
  double zoomChange = 1.0;
  GoogleMapController? mapController;
  double gravityValue = 55;
  LastScreenContainers(
      {super.key,
      required this.color,
      required this.text});

  @override
  Widget build(BuildContext context) {
    final colors = AppTheme.of(context).colors;
    final textTheme = AppTheme.of(context).textTheme;
    return Container(
      
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: colors.colorContainerOnNamedScreen,
          borderRadius: BorderRadius.circular(10.sp)),
      width: 55.w,
      height: 35.h,
      child: Text(text,
          style: textTheme.bodyLarge?.copyWith(color: colors.defaultColorText)),
    );
  }

  void _updateZoom(double newZoom) {
    zoomValue = newZoom;
    if (mapController != null) {
      mapController!.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
            target: LatLng(37.7749, -122.4194),
            zoom: zoomValue,
          ),
        ),
      );
    }
  }
}
