import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../generated/l10n.dart';
import '../../../resources/theme/app_theme.dart';

class HintLastScreen extends StatelessWidget {
  const HintLastScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final colors = AppTheme.of(context).colors;
    final textTheme = AppTheme.of(context).textTheme;
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(left: 30.w),
          alignment: Alignment.centerLeft,
          child: Text(S.current.moveToLastScreen,
              style: textTheme.amountAndAnotherText
                  .copyWith(color: colors.defaultColorText)),
        ),
        SizedBox(
          height: 5.w,
        ),
        Divider(
          color: colors.labelTextAndDivider,
          endIndent: 20.w,
          indent: 20.w,
        ),
        Container(
          margin: EdgeInsets.only(left: 30.w),
          alignment: Alignment.centerLeft,
          child: Text(S.current.moveToLastScreen,
              style: textTheme.amountAndAnotherText
                  .copyWith(color: colors.defaultColorText)),
        ),
      ],
    );
  }
}
