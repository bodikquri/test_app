import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';

import '../../../resources/theme/app_theme.dart';

class WidgeContainerLoginScreen extends StatelessWidget {
  final String text;
  final double height;
  final SvgPicture? image;
  const WidgeContainerLoginScreen({
    super.key,
    required this.text,
    required this.height,
    required this.image,
  });

  @override
  Widget build(BuildContext context) {
    final colors = AppTheme.of(context).colors;
    final textTheme = AppTheme.of(context).textTheme;
    return Container(
        width: 350.w,
        height: 60.h,
        margin: EdgeInsets.only(left: 20.w, right: 20.w, top: height.h),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.5),
              blurRadius: 1,
              offset: Offset(2, 1),
            ),
          ],
          gradient: colors.linearGradient,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            image!,
            SizedBox(
              width: 12.4.h,
            ),
            Text(text,
                style: textTheme.containerText
                    .copyWith(color: colors.defaultColorText))
          ],
        ));
  }
}
