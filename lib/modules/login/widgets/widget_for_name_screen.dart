import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../resources/theme/app_theme.dart';

class WidgetForNamedScreen extends StatelessWidget {
  final String text;
  const WidgetForNamedScreen({super.key, required this.text});
  @override
  Widget build(BuildContext context) {
    final colors = AppTheme.of(context).colors;
    final textTheme = AppTheme.of(context).textTheme;
    return Container(
      margin: EdgeInsets.only(left: 20.w, right: 20.w),
      height: 60.h,
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
              color: colors.dropDownTextColor.withOpacity(0.5),
          blurRadius: 4,
          offset: Offset(2, 2),
        ),
          ],
          color: colors.colorContainerOnNamedScreen,
          borderRadius: BorderRadius.circular(10.sp)),
      child: TextField(
          style:
              textTheme.bodyLarge?.copyWith(color: colors.labelTextAndDivider),
          decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.sp),
                borderSide: BorderSide(color: colors.background)),
            border: const OutlineInputBorder(),
            labelStyle:
                textTheme.body1.copyWith(color: colors.labelTextAndDivider),
            
            label: Container(
              padding: EdgeInsets.only(bottom: 25.h),
              child: Text(
                text,
              ),
            ),
          )),
    );
  }
}
