import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../resources/theme/app_theme.dart';

class TextForNamedScreen extends StatelessWidget {
  final String text;
  const TextForNamedScreen({super.key, required this.text});
  @override
  Widget build(BuildContext context) {
    final colors = AppTheme.of(context).colors;
    final textTheme = AppTheme.of(context).textTheme;
    return Container(
        margin: EdgeInsets.only(
          left: 20.w,
          right: 20.w,
        ),
        alignment: Alignment.centerLeft,
        child: Text(text,
            style: textTheme.textForNamedScreen
                .copyWith(color: colors.defaultColorText)));
  }
}
