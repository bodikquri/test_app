import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../resources/theme/app_theme.dart';

class WidgetContainer extends StatelessWidget {
  final String text;
  const WidgetContainer({super.key, required this.text});
  @override
  Widget build(BuildContext context) {
    final colors = AppTheme.of(context).colors;
    final textTheme = AppTheme.of(context).textTheme;
    return Container(
        width: 350.w,
        height: 60.h,
        margin: EdgeInsets.only(
          left: 20.w,
          right: 20.w,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.sp),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.5),
              blurRadius: 1,
              offset: Offset(2, 1),
            ),
          ],
          gradient: colors.linearGradient,
        ),
        child: Container(
          alignment: Alignment.center,
          child: Text(text,
              style: textTheme.buttonInContainer
                  .copyWith(color: colors.defaultColorText)),
        ));
  }
}
