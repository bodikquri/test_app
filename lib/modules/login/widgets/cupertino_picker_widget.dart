import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../resources/theme/app_theme.dart';

class CupertinoPickerForMyLocationScreen extends StatefulWidget {
  const CupertinoPickerForMyLocationScreen({super.key});

  @override
  State<CupertinoPickerForMyLocationScreen> createState() =>
      _CupertinoPickerForMyLocationScreenState();
}

class _CupertinoPickerForMyLocationScreenState
    extends State<CupertinoPickerForMyLocationScreen> {
  int selectedIndex = 0;
  int index = 0;
  List<Color> colors = [
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
  ];
  @override
  Widget build(BuildContext context) {
    final color = AppTheme.of(context).colors;
    final textTheme = AppTheme.of(context).textTheme;
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          child: SizedBox(
            width: 80.w,
            height: 80.h,
            child: CupertinoPicker(
              itemExtent: 40,
              onSelectedItemChanged: (int index) {
                setState(() {
                  selectedIndex = index;
                  colors = List.generate(
                      21,
                      (int i) =>
                          i == index ? color.textForMyLocation : Colors.white);
                });
              },
              children: <Widget>[
                Text('0', style: TextStyle(color: colors[0])),
                Text('0.5', style: TextStyle(color: colors[1])),
                Text('0.10', style: TextStyle(color: colors[2])),
                Text('0.15', style: TextStyle(color: colors[3])),
                Text('0.20', style: TextStyle(color: colors[4])),
                Text('0.25', style: TextStyle(color: colors[5])),
                Text('0.30', style: TextStyle(color: colors[6])),
                Text('0.35', style: TextStyle(color: colors[7])),
                Text('0.40', style: TextStyle(color: colors[8])),
                Text('0.45', style: TextStyle(color: colors[9])),
                Text('0.50', style: TextStyle(color: colors[10])),
                Text('0.55', style: TextStyle(color: colors[11])),
                Text('0.60', style: TextStyle(color: colors[12])),
                Text('0.65', style: TextStyle(color: colors[13])),
                Text('0.70', style: TextStyle(color: colors[14])),
                Text('0.75', style: TextStyle(color: colors[15])),
                Text('0.80', style: TextStyle(color: colors[16])),
                Text('0.85', style: TextStyle(color: colors[17])),
                Text('0.90', style: TextStyle(color: colors[18])),
                Text('0.95', style: TextStyle(color: colors[19])),
                Text('0.100', style: TextStyle(color: colors[20])),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
