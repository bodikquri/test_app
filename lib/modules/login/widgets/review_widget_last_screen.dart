import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../gen/assets.gen.dart';
import '../../../generated/l10n.dart';
import '../../../resources/theme/app_theme.dart';

class ReviewLastScreen extends StatelessWidget {
  ReviewLastScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final colors = AppTheme.of(context).colors;
    final textTheme = AppTheme.of(context).textTheme;
    return Container(
      width: 350.w,
      height: 175.h,
      decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.black,
              blurRadius: 4,
              offset: Offset(2, 2),
            ),
          ],
          color: colors.colorContainerOnNamedScreen,
          borderRadius: BorderRadius.circular(10.sp)),
      margin: EdgeInsets.only(left: 20.w, right: 20.w),
      child: Column(children: [
        Container(
          padding: EdgeInsets.only(left: 19.w, top: 10.h),
          alignment: Alignment.topLeft,
          child: ShaderMask(
            shaderCallback: (Rect bounds) {
              return LinearGradient(
                colors: [colors.colorForLinearOne, colors.colorForLinearTwo],
              ).createShader(bounds);
            },
            child: Text(S.current.reviewLastScreen,
                style:
                    textTheme.caption.copyWith(color: colors.defaultColorText)),
          ),
        ),
        SizedBox(
          height: 10.h,
        ),
        Container(
          child: Row(
            children: [
              Stack(
                alignment: Alignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 30.w),
                    width: 100.w,
                    height: 20.h,
                    child: Text(S.current.amountLastScreen,
                        style: textTheme.amountAndAnotherText
                            .copyWith(color: colors.defaultColorText)),
                  ),
                  Positioned(
                      right: 24.w,
                      child: MyAssets.images.svg.i
                          .svg(color: colors.defaultColorText)),
                ],
              ),
              SizedBox(
                width: 145.w,
              ),
              Container(
                child: Text(S.current.descriptionForAmount,
                    style: textTheme.amountAndAnotherText
                        .copyWith(color: colors.defaultColorText)),
              )
            ],
          ),
        ),
        SizedBox(
          height: 5.h,
        ),
        Divider(
          color: colors.labelTextAndDivider,
          endIndent: 20.w,
          indent: 20.w,
        ),
        Container(
          child: Row(
            children: [
              Stack(
                alignment: Alignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 30.w),
                    width: 100.w,
                    height: 20.w,
                    child: Text(S.current.gravityLastScreen,
                        style: textTheme.amountAndAnotherText
                            .copyWith(color: colors.defaultColorText)),
                  ),
                  Positioned(
                      right: 32.w,
                      child: MyAssets.images.svg.i
                          .svg(color: colors.defaultColorText)),
                ],
              ),
              SizedBox(
                width: 165.w,
              ),
              Container(
                child: Text(S.current.gravityvalue,
                    style: textTheme.amountAndAnotherText
                        .copyWith(color: colors.defaultColorText)),
              )
            ],
          ),
        ),
        SizedBox(
          height: 5.h,
        ),
        Divider(
          color: colors.labelTextAndDivider,
          endIndent: 20.w,
          indent: 20.w,
        ),
        Container(
          child: Row(
            children: [
              Stack(
                alignment: Alignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 30.w),
                    width: 100.w,
                    height: 20.h,
                    child: Text(S.current.priceLastScreen,
                        style: textTheme.amountAndAnotherText
                            .copyWith(color: colors.defaultColorText)),
                  ),
                  Positioned(
                      right: 47.w,
                      child: MyAssets.images.svg.i
                          .svg(color: colors.defaultColorText)),
                ],
              ),
              SizedBox(
                width: 155.w,
              ),
              Container(
                child: Text('0.75\$',
                    style: textTheme.amountAndAnotherText
                        .copyWith(color: colors.defaultColorText)),
              )
            ],
          ),
        ),
      ]),
    );
  }


}

