import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:test_app/modules/login/cubit/login/login_cubit.dart';
import 'package:test_app/modules/login/login_routes_config.dart';
import 'package:test_app/modules/login/screens/login_screen.dart';

class LoginModule extends Module {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => LoginRoutesConfig(),
        ),
        Bind(
          (i) => LoginCubit(),
        ),
        Bind.factory(
          (i) => TextEditingController(),
        ),
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute(
          LoginRoutesConfig.schemes.login,
          child: (_, __) => LoginScreen(),
        ),
      ];
}
