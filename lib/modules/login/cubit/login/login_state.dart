part of 'login_cubit.dart';

class AlertDialogState extends LoginState {}

class InitialState extends LoginState {}

class LaterLoginState extends LoginState {}

class LoadingState extends LoginState {}

class LoginEmailState extends LoginState {}

class LoginFacebookState extends LoginState {}

class LoginGoogleState extends LoginState {}

abstract class LoginState extends Equatable {
  const LoginState();

  @override
  List<Object> get props => [DateTime.now().toIso8601String()];
}

class OpenState extends LoginState {}

class SuccessState extends LoginState {}
