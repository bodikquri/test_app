import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  Map<String, StreamSubscription> subscriptions = {};

  final name = "lol";

  LoginCubit() : super(InitialState());

  @override
  Future<void> close() {
    _cancelSubscriptions();
    return super.close();
  }

  loginEmail() {
    emit(LoginEmailState());
  }

  loginFacebook() {
    emit(LoginFacebookState());
  }

  loginGoogle() {
    emit(LoginGoogleState());
  }

  void loginLater() {
    emit(LaterLoginState());
  }

  void ontap() {}

  void _cancelSubscriptions() {
    subscriptions.forEach((key, value) {
      value.cancel();
    });
  }
}
