part of 'named_cubit.dart';

class AlertDialogState extends NamedState {}

class InitialState extends NamedState {}

class LaterLoginState extends NamedState {}

class LoadingState extends NamedState {}

class LoginEmailState extends NamedState {}

class LoginFacebookState extends NamedState {}

class LoginGoogleState extends NamedState {}

abstract class NamedState extends Equatable {
  const NamedState();

  @override
  List<Object> get props => [DateTime.now().toIso8601String()];
}

class OpenState extends NamedState {}

class SuccessState extends NamedState {}
