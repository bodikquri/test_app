import 'package:test_app/modules/routes_interface.dart';

class LoginRoute extends RouteDestination {
  static String scheme = LoginRoutesConfig.route + 'loginScreen';

  @override
  String path() => LoginRoutesConfig.moduleRoute + 'loginScreen';
}

class LoginRoutesConfig {
  static Schemes schemes = Schemes();
  static String route = '/';
  static String moduleRoute = '/login/';
}

class Schemes {
  final login = LoginRoute.scheme;
}
