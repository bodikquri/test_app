import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:test_app/modules/home/screens/dashboard/dashboard_screen.dart';

import 'cubit/dashboard_cubit.dart';
import 'home_routes_config.dart';

class HomeModule extends Module {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => HomeRoutesConfig(),
        ),
        Bind.factory(
          (i) => TextEditingController(),
        ),
        Bind.factory(
          (i) => FocusNode(),
        ),
       
        Bind(
          (i) => DashboardCubit(),
        ),
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute(HomeRoutesConfig.schemes.dashboard,
            child: (_, __) => DashBoardScreen(),
            children: [
              ChildRoute(HomeRoutesConfig.schemes.home,
                  child: (_, __) => Container()),
            ]),
      ];
}
