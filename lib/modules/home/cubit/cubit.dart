import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'state.dart';

class BaseCubit extends Cubit<BaseState> {
  Map<String, StreamSubscription> subscriptions = {};

  BaseCubit() : super(InitialState());

  @override
  Future<void> close() {
    _cancelSubscriptions();
    return super.close();
  }

  void _cancelSubscriptions() {
    subscriptions.forEach((key, value) {
      value.cancel();
    });
  }
}
