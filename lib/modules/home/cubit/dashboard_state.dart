part of 'dashboard_cubit.dart';

class AlertDialogState extends DashboardState {}

abstract class DashboardState extends Equatable {
  const DashboardState();

  @override
  List<Object> get props => [DateTime.now().toIso8601String()];
}

class InitialState extends DashboardState {}

class LoadingState extends DashboardState {}

class OpenState extends DashboardState {}

class SuccessState extends DashboardState {}
