part of 'cubit.dart';

class AlertDialogState extends BaseState {}

abstract class BaseState extends Equatable {
  const BaseState();

  @override
  List<Object> get props => [DateTime.now().toIso8601String()];
}

class InitialState extends BaseState {}

class LoadingState extends BaseState {}

class OpenState extends BaseState {}

class SuccessState extends BaseState {}
