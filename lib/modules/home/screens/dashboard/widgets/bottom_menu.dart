import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:test_app/modules/home/cubit/dashboard_cubit.dart';

class BottomBar extends StatelessWidget {
  const BottomBar({
    Key? key,
  }) : super(key: key);

  DashboardCubit get dashboardCubit => Modular.get<DashboardCubit>();

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
