import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:test_app/modules/home/cubit/dashboard_cubit.dart';
import 'package:test_app/utils/navigate_mixin.dart';

class DashBoardScreen extends StatelessWidget with NavigateMixin {
  DashBoardScreen({Key? key}) : super(key: key);

  DashboardCubit get cubit => Modular.get<DashboardCubit>();

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<DashboardCubit, DashboardState>(
        bloc: cubit,
        listener: (context, state) async {},
        builder: (context, state) {
          return WillPopScope(
            onWillPop: () async {
              navigatePop();
              return false;
            },
            child: Scaffold(
              body: RouterOutlet(),
            ),
          );
        });
  }
}
