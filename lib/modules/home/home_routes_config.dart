import 'package:test_app/modules/routes_interface.dart';

class DashBoardRoute extends RouteDestination {
  static String scheme = HomeRoutesConfig.route + "dashboard";

  static String moduleRoute = HomeRoutesConfig.moduleRoute + "dashboard";

  @override
  String path() => HomeRoutesConfig.moduleRoute + "dashboard";
}

class HomeRoute extends RouteDestination {
  static String scheme = HomeRoutesConfig.route + 'home';

  @override
  String path() => DashBoardRoute.moduleRoute + '/home';
}

class HomeRoutesConfig {
  static Schemes schemes = Schemes();
  static String route = '/';
  static String moduleRoute = '/home/';
}

class Schemes {
  final home = HomeRoute.scheme;
  final dashboard = DashBoardRoute.scheme;
}
