/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/services.dart';

class $AssetsDataGen {
  const $AssetsDataGen();

  /// File path: assets/data/countries.json
  String get countries => 'assets/data/countries.json';

  /// File path: assets/data/currency.json
  String get currency => 'assets/data/currency.json';

  /// List of all assets
  List<String> get values => [countries, currency];
}

class $AssetsImagesGen {
  const $AssetsImagesGen();

  $AssetsImagesSvgGen get svg => const $AssetsImagesSvgGen();
}

class $AssetsImagesSvgGen {
  const $AssetsImagesSvgGen();

  /// File path: assets/images/svg/Vector.svg
  SvgGenImage get vector => const SvgGenImage('assets/images/svg/Vector.svg');

  /// File path: assets/images/svg/i.svg
  SvgGenImage get i => const SvgGenImage('assets/images/svg/i.svg');

  /// File path: assets/images/svg/ic_google.svg
  SvgGenImage get icGoogle =>
      const SvgGenImage('assets/images/svg/ic_google.svg');

  /// File path: assets/images/svg/icon_for_named_screen.svg
  SvgGenImage get iconForNamedScreen =>
      const SvgGenImage('assets/images/svg/icon_for_named_screen.svg');

  /// File path: assets/images/svg/line_for_loc_screen.svg
  SvgGenImage get lineForLocScreen =>
      const SvgGenImage('assets/images/svg/line_for_loc_screen.svg');

  /// File path: assets/images/svg/line_for_loc_screen_two.svg
  SvgGenImage get lineForLocScreenTwo =>
      const SvgGenImage('assets/images/svg/line_for_loc_screen_two.svg');

  /// File path: assets/images/svg/line_for_named.svg
  SvgGenImage get lineForNamed =>
      const SvgGenImage('assets/images/svg/line_for_named.svg');

  /// File path: assets/images/svg/line_log_screen.svg
  SvgGenImage get lineLogScreen =>
      const SvgGenImage('assets/images/svg/line_log_screen.svg');

  /// File path: assets/images/svg/line_ones.svg
  SvgGenImage get lineOnes =>
      const SvgGenImage('assets/images/svg/line_ones.svg');

  /// File path: assets/images/svg/line_second.svg
  SvgGenImage get lineSecond =>
      const SvgGenImage('assets/images/svg/line_second.svg');

  /// File path: assets/images/svg/line_three.svg
  SvgGenImage get lineThree =>
      const SvgGenImage('assets/images/svg/line_three.svg');

  /// File path: assets/images/svg/my_location.svg
  SvgGenImage get myLocation =>
      const SvgGenImage('assets/images/svg/my_location.svg');

  /// File path: assets/images/svg/phone.svg
  SvgGenImage get phone => const SvgGenImage('assets/images/svg/phone.svg');

  /// File path: assets/images/svg/shadow_log_screen.svg
  SvgGenImage get shadowLogScreen =>
      const SvgGenImage('assets/images/svg/shadow_log_screen.svg');

  /// File path: assets/images/svg/shadow_second.svg
  SvgGenImage get shadowSecond =>
      const SvgGenImage('assets/images/svg/shadow_second.svg');

  /// File path: assets/images/svg/shadow_with_truck.svg
  SvgGenImage get shadowWithTruck =>
      const SvgGenImage('assets/images/svg/shadow_with_truck.svg');

  /// File path: assets/images/svg/splash_truck.svg
  SvgGenImage get splashTruck =>
      const SvgGenImage('assets/images/svg/splash_truck.svg');

  /// File path: assets/images/svg/truck.svg
  SvgGenImage get truck => const SvgGenImage('assets/images/svg/truck.svg');

  /// List of all assets
  List<SvgGenImage> get values => [
        vector,
        i,
        icGoogle,
        iconForNamedScreen,
        lineForLocScreen,
        lineForLocScreenTwo,
        lineForNamed,
        lineLogScreen,
        lineOnes,
        lineSecond,
        lineThree,
        myLocation,
        phone,
        shadowLogScreen,
        shadowSecond,
        shadowWithTruck,
        splashTruck,
        truck
      ];
}

class MyAssets {
  MyAssets._();

  static const $AssetsDataGen data = $AssetsDataGen();
  static const $AssetsImagesGen images = $AssetsImagesGen();
}

class AssetGenImage {
  const AssetGenImage(this._assetName);

  final String _assetName;

  Image image({
    Key? key,
    AssetBundle? bundle,
    ImageFrameBuilder? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Animation<double>? opacity,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    String? package,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
  }) {
    return Image.asset(
      _assetName,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale,
      width: width,
      height: height,
      color: color,
      opacity: opacity,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      package: package,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  ImageProvider provider({
    AssetBundle? bundle,
    String? package,
  }) {
    return AssetImage(
      _assetName,
      bundle: bundle,
      package: package,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}

class SvgGenImage {
  const SvgGenImage(this._assetName);

  final String _assetName;

  SvgPicture svg({
    Key? key,
    bool matchTextDirection = false,
    AssetBundle? bundle,
    String? package,
    double? width,
    double? height,
    BoxFit fit = BoxFit.contain,
    AlignmentGeometry alignment = Alignment.center,
    bool allowDrawingOutsideViewBox = false,
    WidgetBuilder? placeholderBuilder,
    String? semanticsLabel,
    bool excludeFromSemantics = false,
    SvgTheme theme = const SvgTheme(),
    ColorFilter? colorFilter,
    Clip clipBehavior = Clip.hardEdge,
    @deprecated Color? color,
    @deprecated BlendMode colorBlendMode = BlendMode.srcIn,
    @deprecated bool cacheColorFilter = false,
  }) {
    return SvgPicture.asset(
      _assetName,
      key: key,
      matchTextDirection: matchTextDirection,
      bundle: bundle,
      package: package,
      width: width,
      height: height,
      fit: fit,
      alignment: alignment,
      allowDrawingOutsideViewBox: allowDrawingOutsideViewBox,
      placeholderBuilder: placeholderBuilder,
      semanticsLabel: semanticsLabel,
      excludeFromSemantics: excludeFromSemantics,
      theme: theme,
      colorFilter: colorFilter,
      color: color,
      colorBlendMode: colorBlendMode,
      clipBehavior: clipBehavior,
      cacheColorFilter: cacheColorFilter,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}
