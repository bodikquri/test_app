import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:logger/logger.dart';
import 'package:test_app/app.dart';
import 'package:test_app/modules/app_module.dart';

import 'firebase_options.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
 
  await SystemChrome.setPreferredOrientations(
    [DeviceOrientation.portraitUp],
  );
  final appModule = AppModule();

  runApp(
    ModularApp(module: appModule, child: const App()),
  );
}

const String ENGLISH_LOCALE_CODE = 'en';

const Map<String, Locale> supportedLocales = {
  ENGLISH_LOCALE_CODE: Locale(ENGLISH_LOCALE_CODE),
};

const String UK_COUNTRY_CODE = 'united_kingdom';

final logger = Logger(printer: PrettyPrinter());
